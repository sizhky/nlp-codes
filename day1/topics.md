## Topics Covered

* Loading Texts
* Import nltk
* Stop word removal
* Special character removal (corpus cleaning)
* Lower case
* Sentence Tokenizing
* Word Tokenizing
* N-Grams  


* Solr basic search

## Activity
* Given multiple webpages, clean the text and print the most common n-grams of each category
