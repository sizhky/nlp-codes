
# Cognizant NLP Training

## 27th Nov	Day 1
* Introduction to text mining 
    - **Basic exercises in python. Loading nltk -> Ways of reading multiple text files -> Basic preprocessing (stop words, lower, tokenization, sentence tokenization)** 

* BOW 
    - **There are two ways to create a sentence from bag of words. Multiple Hot Encoding and TFIDF. These shall be introduced as concepts**
* N-grams 
    - **nltk activity -> Load a Shakespear corpus and build a bi-gram language model. At end, we can generate a text that reads like Shakespear. But the problem is Language modelling doesn't come till 3rd day, so I am not sure what else N-Grams are used for**

* text to vector space models 
    - **This is covered by BOW activity**

* Basics of text preprocessing; Application: Given a corpus of text documents participants will have to process the data using pre-processing steps;  
    - **An activity will be given that uses all the above concepts (except tfidf)**

* Indexing & Search
* Inverted index
* TF-IDF with Solr.
    - I still haven't gone through the full tutorial. Simple search is possible.
    - I'm not sure why they'd want to access the tfidf. **UPDATE: ACCESSING TFIDF IS NOT POSSIBLE**

## 28th Nov	Day 2
* Basics of probability theory
    - **I would like your inputs on this. I don't understand what probability we can introduce in text mining that is unrelated to markov chains or N-Grams**
* Bayes Theorem
    - **There are enough examples of bayes theorem but are unrelated to text. I don't think there would be an activity on this.**
* Naïve Bayes Classifier. 
    - Applications: binary class and multi-class problems. 
    - **Read a bunch of movie reviews and classify if the review is positive or not;  Code for this is ready**
* Provide various distance metrics 
    - [sklearn] - **I'm not sure why**
* Variety of clustering techniques. 
    - [sklearn] - **Cluster a corpus of mixture English and German documents into two clusters**
* Given a corpus of documents apply K-means to find similar documents. 
    - [sklearn] - **Same corpus can be used**

## 29th Nov	Day 3
* Introduction to Markov modeling
* Language Modeling and information extraction
* Hidden Markov Models
    - [Markovify's documentation](https://github.com/jsvine/markovify) seems very promising. I might use it. **I still haven't started with this**
* POS tagging 
    - **Naive Bayes classifier from previous class. But now the features extracted will be only adjectives**
* Named Entity Extraction 
    - **NLTK Activity**
* Topic Modeling with `Gensim` 
    - **To be figured out. But I am sure, there are plenty of corpora.**


## 30th Nov	Day 4 
#### This is a full day lecture according excel sheet. Is it full day lab or full day lecture??
* Word Sense Disambiguation and predicting next term using these methods given a corpus of documents.
    - **From what I understood, there is nothing different between POS tagging and WSD. All one is trying to figure out is a clearer sense of the word. We could introduce WordNet research which is about finding several word meanings/'sense' and identifying their sentiment & subjectivity**
    - **I am concerned how we could fit such a small topic for 8 hours. Could you provide your thoughts on this, Dr. Manoj?**
    - **I could fit in the `spacy` nlp package in the time here though**

## 1st Dec	Day 5 
* Neural Networks Introduction
* Importance of Word2Vec
    - **A demo on loading Glove vectors and finding relation (like `man - woman + queen = ?`) between similar words is ready**
* Feature extraction and its applications 
    - **A code on MLP with Glove vectors is ready and due to be demonstrated to you**


* Brief about the project: multi-class classification problem statement
    objective and benchmark accuracies.
    - I have a dataset of blogs in my mind. The task there is to identify the author's profile (age, gender, profession are some of the targets we have).
    - **Still yet to figure out what needs to be done. A dataset for patient classification is available with me, that was used as Final Project for previous CPEE batches though.** 
